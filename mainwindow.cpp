#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QProcess>

#include <QStringList>
#include <iostream>
#include <QDebug>

#include "mytablemodel.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    loadProcessList();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadProcessList()
{
//    QString program = "wmic";
//    QStringList args = {
//        "/OUTPUT:STDOUT",
//        "PROCESS",
//        "GET",
//        "ProcessId, Status, Caption, InstallDate, ExecutablePath, VirtualSize",
//        "/FORMAT:CSV"
//    };

//    QString program = "tasklist";
//    QStringList args = {"/v", "/fo", "csv"};

    QString program = "wmic";
    QStringList args = {"process", "get", "/format:csv"};

    QProcess process;
    process.start(program, args);
    QString content = QString::null;
    while (process.waitForReadyRead())
    {
        content += process.readAll();
    }
    std::cout << content.toStdString() << std::endl;

    QVector<QVector<QVariant>> tableValues;

    QStringList processes = content.split('\n');
    processes.removeFirst();
    processes.removeLast();
    tableValues.resize(processes.count());
    int row = 0;
    for (QString process : processes) {
        QStringList processValues = process.split(',');
        tableValues[row].resize(processValues.count());
        int col = 0;
        for (QString processValue : processValues) {
            tableValues[row][col] = processValue;
            ++col;
        }
        ++row;
    }

    MyTableModel * myModel = new MyTableModel(this);
    myModel->setDataSource(tableValues);
    ui->mTableView->setModel(myModel);
    ui->mTableView->resizeColumnsToContents();
}
